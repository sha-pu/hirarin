#pragma once

#include "Common.h"
#include "DxLib.h"

class CAfterManager;

class CEnemy :public CCommon
{
public:
	//CAfterManager *afterManager;
	//void SetAfterManager(CAfterManager *after) { afterManager = after; }
	//移動ルーチン
	virtual void Move()=0;

	//生存フラグを返す
	bool GetFlag() { return flag; };
	//移動速度を返す
	VECTOR GetVPos() { return vPos; };
	//目的座標をセット
	void SetPlaPos(const VECTOR &pPlaPos) { plaPos = pPlaPos; };
	SColor eColor;
	//目的座標
	VECTOR plaPos;
};

class EnemyA:public CEnemy
{
public:
	EnemyA(const VECTOR *pPos);
	~EnemyA() {};

	void Update();
	void Render();
	void Move();
};