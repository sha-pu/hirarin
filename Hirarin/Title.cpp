#include"Manager.h"
#include"Game.h"
#include"Define.h"
#include "Title.h"


void CTitle::Update()
{
	if (manager->GetMouse()[MOUSE_LEFT] == 1)
	{
		manager->scene = new CGame(manager);
		delete this;
	}
}

void CTitle::Render()
{
	DrawFormatString(SCREEN_W_HALF,SCREEN_H_HALF, GetColor(255, 255, 255), "タイトル");
	DrawFormatString(SCREEN_W_HALF, SCREEN_H_HALF+32, GetColor(255, 255, 255), "左クリックでゲームへ");
}
