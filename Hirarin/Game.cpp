#include "Player.h"
#include "Manager.h"
#include "Game.h"
#include "AfterManager.h"
#include "EnemyManager.h"
#include "Collision.h"
#include "Clear.h"
#include "Over.h"

CGame::CGame(CManager *pManager) :CScene(pManager)
{
	player = new CPlayer(pManager);
	afterManager = new CAfterManager();
	enemyManager = new CEnemyManager();
	player->SetAfterManager(afterManager);
	enemyManager->SetAfterManager(afterManager);
	collision = new CCollision();
}

CGame::~CGame()
{
	delete player;
	delete afterManager;
	delete enemyManager;
	delete collision;
}

void CGame::Update()
{
	player->Update();

	enemyManager->Update();
	VECTOR *pPos = &(player->GetPos());
	enemyManager->SetPlaPos(*pPos);

	enemyManager->Spawn();
	for (int i = 0; i < ENEMY_NUM; i++)
	{
		if (enemyManager->GetEnemy(i) != NULL)
		{
			//プレイヤーと敵の当たり判定
			collision->CheckCollision(player, (enemyManager->GetEnemy(i)));
			for (int j = 0; j < ENEMY_NUM; j++)
			{
				if (enemyManager->GetEnemy(j) != NULL)
				{
					if (i != j)
					{
						//エネミー同士の当たり判定
						collision->CheckCollision((enemyManager->GetEnemy(i)), (enemyManager->GetEnemy(j)));
					}
				}
			}
		}
	}
	SceneChange();
}

void CGame::Render()
{
	DrawFormatString(0, 0, GetColor(255, 255, 255), "ゲーム");
	DrawFormatString(100, 0, GetColor(255, 255, 255), "x<=100,y<=100で左クリックするとクリアへ移行(debug),敵と接触するとオーバー,敵と敵を接触させると倒すことができる");
	player->Render();
	enemyManager->Render();
}

void CGame::SceneChange()
{
	if (Debug_Scene() == true)
	{
		manager->scene = new CClear(manager);
		delete this;
	}
	else if (player->GetFlag() == false)
	{
		manager->scene = new COver(manager);
		delete this;
	}
}

bool CGame::Debug_Scene()
{
	VECTOR *mPos = &(manager->GetMousePos());
	VECTOR zPos = VGet(0, 0, 0);
	if (manager->GetMouse()[MOUSE_LEFT] == 1 &&
		(*mPos).x <= 100 &&
		(*mPos).y <= 100)
	{
		return true;
	}
	return false;
}
