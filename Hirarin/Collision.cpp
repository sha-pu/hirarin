#include "Collision.h"
#include "Common.h"

void CCollision::CheckCollision(CCommon *o1, CCommon *o2)
{
	VECTOR obj1Pos = o1->GetPos();
	VECTOR obj2Pos = o2->GetPos();
	float obj1Range = o1->GetRange();
	float obj2Range = o2->GetRange();

	float distance = VSquareSize(VSub(obj1Pos, obj2Pos));
	float range = (obj1Range + obj2Range)*(obj1Range + obj2Range);

	if (distance < range)
	{
		o1->HitAction();
		o2->HitAction();
	}
}