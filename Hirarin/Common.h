#pragma once
#include "DxLib.h"
#include "Define.h"
//#include "AfterManager.h"
class CAfterManager;

//継承用
class CCommon
{
protected:
	CAfterManager *afterManager;

	//座標のベクトル情報
	VECTOR pos;
	//移動速度のベクトル情報
	VECTOR vPos;
	//仮想関数
	virtual void Update() = 0;
	virtual void Render() = 0;
public:
	void SetAfterManager(CAfterManager *after) { afterManager = after; }
	//現在地を返す
	VECTOR GetPos() { return pos; };
	//現座標の小数点以下を全て0にする
	void posIntChange(float *p);
	void Spawn(VECTOR *pPos, VECTOR *pVPos, SColor *pColor, float Count);
	//生存フラグ。生存=true;
	bool flag;
	void HitAction() { flag = false; };
	float GetRange(){return range;};
	float range;
};

