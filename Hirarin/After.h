#pragma once

//#include "Common.h"
#include "Define.h"
#include "DxLib.h"

class CAfter/* :public CCommon*/
{
public:
	//呼び出されたときに与えられた座標から与えられた移動速度を反転させて進み
	//移動速度を与えられなかった場合その場で停止する
	CAfter(VECTOR *pPos, SColor *pColor, float pCountMin);
	CAfter(VECTOR *pPos, VECTOR *pVPos, SColor *pColor, float pCountMin);
	~CAfter() {};

	void Update();
	void Render();
	int GetCnt() { return cnt; }
	VECTOR GetVPpos() { return vPos; }
	float GetCountMin() { return countMin; }
	
private:
	VECTOR pos;
	VECTOR vPos;

	int cnt;
	float countMin;
	SColor color;
};
