#pragma once

//マウスのボタン
#define MOUSE_NUM 2
#define MOUSE_LEFT 0
#define MOUSE_RIGHT 1
//スクリーン情報
#define SCREEN_WIDTH  1280
#define SCREEN_HEIGHT 960
#define SCREEN_W_HALF SCREEN_WIDTH/2
#define SCREEN_H_HALF SCREEN_HEIGHT/2
/************************************/
//プレイヤー
#define P_X 100
#define P_Y 100
#define P_SPEED 5
//プレイヤーの色
#define P_COLOR 125,125,255
#define P_COLOR_R 125
#define P_COLOR_G 125
#define P_COLOR_B 255
//#define P_A_COUNT 60
//sAfterが消えるまでのカウント
#define P_SA_CNT 60
//カウントの誤差
#define P_SA_CNT_RAND 60
//1Fで出るAfterの最大数
#define P_A_RAND 10
/************************************/
//エネミー
#define E_SPEED 4.1
//エネミーの色
#define E_COLOR 255,255,255
#define E_COLOR_R 255
#define E_COLOR_G 255
#define E_COLOR_B 255
#define E_SA_CNT 0
/************************************/

//SAfterの幅
//#define GAP 0.4
//SAfterの本数
#define FUN_NUM 8

//AfterManager.hで宣言、Afterの最大値
//#define AFTER_NUM 400

struct SColor
{
	int red;
	int green;
	int blue;
};