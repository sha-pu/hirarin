#include"Manager.h"
#include"Title.h"
#include"Define.h"
#include"Clear.h"

void CClear::Update()
{
	if (manager->GetMouse()[MOUSE_RIGHT] == 1 )
	{
		manager->scene=new CTitle(manager);
		delete this;
	}
}

void CClear::Render()
{
	DrawFormatString(SCREEN_W_HALF, SCREEN_H_HALF, GetColor(255, 255, 255), "クリア");
	DrawFormatString(SCREEN_W_HALF, SCREEN_H_HALF+32, GetColor(255, 255, 255), "右クリックでタイトルへ");

}
