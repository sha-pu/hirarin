#pragma once
#include "After.h"

//class CAfter;

struct SColor;
#define AFTER_NUM 15000

class CAfterManager
{
	CAfter *after[AFTER_NUM];

public:
	CAfterManager();
	~CAfterManager();

	//呼び出されたときに与えられた座標から与えられた移動速度を反転させて進み
	//移動速度を与えられなかった場合その場で停止する
	void Spawn(VECTOR *pPos,SColor *pColor,float pCountMin);
	void Spawn(VECTOR *pPos, VECTOR *vPos, SColor *pColor, float pCountMin);

	//継承
	void Update();
	void Render();

	//与えられた座標を中心とし、移動速度をxpixel離れたとこにrad°回転させた値をsPosに代入する
	void rotation(VECTOR *sPos, const VECTOR *pos, const VECTOR *vPos, float rad,float x);
private:
	//今のAfterのカウントを調べる
	void CountCheck();
};