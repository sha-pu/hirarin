#include "Player.h"
#include "Manager.h"
#include "math.h"
#include "AfterManager.h"


CPlayer::CPlayer(CManager *pManager)
{
	manager = pManager;
	pos = VGet(P_X, P_Y, 0);
	//x = 5;
	//y = 0;
	flag = true;
	spawnCnt = 0;
	pColor = { P_COLOR_R,P_COLOR_G,P_COLOR_B };
	range=3;
}

CPlayer::~CPlayer()
{

}

void CPlayer::Update()
{

	Move();
	afterManager->Update();
}


void CPlayer::Move()
{
	double vx = 0, vy = 0;
	//マウスの座標に向けて一定速度で進む
	VECTOR targetPos = manager->GetMousePos();
	double distance = sqrt((targetPos.x - pos.x)*(targetPos.x - pos.x) + (targetPos.y - pos.y)*(targetPos.y - pos.y));
	if (distance)
	{
		vx = (targetPos.x - pos.x) / distance * P_SPEED;
		vy = (targetPos.y - pos.y) / distance * P_SPEED;
	}
	vPos = VGet((float)vx, (float)vy, 0);
	//マウス座標を追いかけ続けると小数点を埋めることができずガクガクするのでint型にして埋めやすくする
	posIntChange(&vPos.x);
	posIntChange(&vPos.y);
	pos = VAdd(pos, vPos);

	//コントローラーで操作
	////int _rotation=0;
	////_rotation=atan2()
	////vx = 5 * cos(5 / 180 * 3.14);
	////vy = 5 * sin(5 / 180 * 3.14);
	//int bx = x;
	//int by = y;
	//GetJoypadAnalogInput(&x, &y, DX_INPUT_PAD1);
	//x /= 200;
	//y /= 200;
	//if (x == 0 && y == 0)
	//{
	//	x = bx;
	//	y = by;
	//}
	//	vx += x * 1;
	//	vy += y * 1;
}



void CPlayer::Render()
{
	AfterSpawn();
	VECTOR a = VGet(0, 0, 0);
	for (float i = -180; i <= 180; i += 30)
	{
		for (float j = 1; j <= 3; j++)
		{
			afterManager->rotation(&a, &pos, &pos, i, j);
			DrawPixel((int)a.x, (int)a.y, GetColor(P_COLOR));
		}
	}
	DrawPixel((int)pos.x, (int)pos.y, GetColor(P_COLOR));
	afterManager->Render();
}

void CPlayer::AfterSpawn()
{
	spawnCnt += 10;

	//30Fごとに軌跡にAfterを出す
	if (spawnCnt % 30 == 0)
	{
		AfterLocus();
		spawnCnt = 0;
	}
	//2Fごとに自分のvPosと逆方向にAfterを発射させる
	if (spawnCnt % 2 == 0)
	{
		AfterRand();
	}
		//プレイヤーの周辺に点を書く
		//for (int i = 0; i < 360; i += 15)
		//{
		//	VECTOR rad;
		//	rad = (VGet(pos.x + (float)cos(i) * 50, pos.y + (float)sin(i) * 50, 0));
		//	afterManager->Spawn(&rad);
		//}
}

void CPlayer::AfterLocus()
{
	VECTOR a = VGet(0, 0, 0);
	for (float i = -180; i <= 180; i += 90)
	{
		for (float j = 1; j <= 3; j++)
		{
			afterManager->rotation(&a, &pos, &pos, i, j);
			afterManager->Spawn(&a,&pColor,P_SA_CNT);
		}
	}
}

void CPlayer::AfterRand()
{
	VECTOR vPosSave[FUN_NUM];

	int sisu = 0;
	for (float i = 15; i <= 60; i += 15)
	{
		for (float j = -5; j <= 5; j += 10)
		{
			VECTOR pPPos = vPos;
			afterManager->rotation(&pPPos, &pos, &vPos, i, j);
			vPosSave[sisu] = pPPos;
			sisu++;
		}
	}
	int vol = GetRand(P_A_RAND);
	for (float i = 0; i < vol; i++)
	{
		VECTOR aPos = vPosSave[GetRand(FUN_NUM - 1)];
		Spawn(&pos, &aPos);
	}
}

void CPlayer::Spawn(VECTOR *pPos, VECTOR *pVPos)
{
	afterManager->Spawn(pPos, pVPos, &pColor, P_SA_CNT);
}