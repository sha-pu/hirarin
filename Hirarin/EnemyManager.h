#pragma once

#include"DxLib.h"
//#include"Common.h"
class EnemyA;
class CAfterManager;
class CCommon;
#define ENEMY_NUM 10

class CEnemyManager
{
	EnemyA *enemy[ENEMY_NUM];
	//EnemyManager側でAfterManagerを動的確保させる
	CAfterManager *afterManager;
public:
	void SetAfterManager(CAfterManager *after) { afterManager = after; }
	CEnemyManager();
	~CEnemyManager();

	//Enemyを湧かせる関数
	void Spawn();
	//Afterを湧かせるための準備
	//void AfterSpawn();

	//継承
	void Update();
	void Render();
	//目的座標をセットする
	void SetPlaPos(const VECTOR &pPlaPos) { plaPos = pPlaPos; };
	//エネミーをCCommon型にキャストして返す
	CCommon *GetEnemy(int num) { return (CCommon*)enemy[num]; }
private:
	//目的座標
	VECTOR plaPos;
	//Afterを湧かせる関数
	//void Spawn(VECTOR *pPos,VECTOR *pVPos);
	//Enemyが湧くカウント
	int cnt;
	//上下左右どの場所から湧くかをランダムで決める
	void SpawnPos(double *width,double *height);
};