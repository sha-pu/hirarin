#include "Enemy.h"
#include "Define.h"
#include "math.h"
#include "AfterManager.h"

EnemyA::EnemyA(const VECTOR *pPos)
{
	pos = *pPos;
	plaPos = VGet(0, 0, 0);
	flag = true;
	eColor = { E_COLOR_R,E_COLOR_G,E_COLOR_B };
	range=2;
}

void EnemyA::Update()
{
	Move();
	VECTOR vPosSave[FUN_NUM];

	int sisu = 0;
	for (float i = 15; i <= 60; i += 15)
	{
		for (float j = -5; j <= 5; j += 10)
		{
			VECTOR pPPos = vPos;
			afterManager->rotation(&pPPos, &pos, &vPos, i, j);
			vPosSave[sisu] = pPPos;
			sisu++;
		}
	}
	int vol = GetRand(P_A_RAND);
	for (float i = 0; i < vol; i++)
	{
		VECTOR aPos = vPosSave[GetRand(FUN_NUM - 1)];
		Spawn(&pos, &aPos,&eColor,E_SA_CNT);
	}
}

void EnemyA::Move()
{
	//目的座標に向けて一定速度で進む
	double vx = 0, vy = 0;
	double distance = sqrt((plaPos.x - pos.x)*(plaPos.x - pos.x) + (plaPos.y - pos.y)*(plaPos.y - pos.y));
	if (distance)
	{
		vx = (plaPos.x - pos.x) / distance * E_SPEED;
		vy = (plaPos.y - pos.y) / distance * E_SPEED;
	}
	posIntChange(&vPos.x);
	posIntChange(&vPos.y);
	vPos = VGet((float)vx, (float)vy, 0);
	pos = VAdd(pos, vPos);
	
}

void EnemyA::Render()
{
	DrawPixel((int)pos.x, (int)pos.y, GetColor(E_COLOR));
}
