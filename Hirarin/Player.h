#pragma once

#include "Common.h"
#include "Define.h"

class CManager;
//class CAfterManager;
//struct SColor;

class CPlayer:public CCommon
{
	CManager *manager;
	//CAfterManager *afterManager;
public:
	CPlayer(CManager *);
	~CPlayer();

	//継承
	void Update();
	void Render();
	//Player側でAfterManagerを動的確保させる
	//void SetAfterManager(CAfterManager *after) { afterManager = after; }
	bool GetFlag(){return flag;}
private:
	//移動処理
	void Move();
	//Afterが湧くカウント
	int spawnCnt;
	//Afterを湧かせる準備
	void AfterSpawn();
	//Afterを湧かせる関数
	void Spawn(VECTOR *pPos,VECTOR *pVPos);
	//自分の軌跡にAfterを残す
	void AfterLocus();
	//Afterが進むベクトル情報にランダムで発射する
	void AfterRand();
	//プレイヤーの色。Afterに渡して色を変えたりする
	SColor pColor;


	//void rotation(VECTOR ro,float rad);
	//int x;
	//int y;
};