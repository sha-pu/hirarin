#include "Common.h"
#include "AfterManager.h"

void CCommon::posIntChange(float *p) 
{
	int position = (int)*p;
	*p = (float)position;
};

void CCommon::Spawn(VECTOR *pPos, VECTOR *pVPos,SColor *pColor,float Count)
{
	afterManager->Spawn(pPos, pVPos, pColor, Count);
}