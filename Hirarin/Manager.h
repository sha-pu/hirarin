#pragma once
#include "DxLib.h"

class CManager;

//シーンの基底クラス
class CScene
{
protected:
	CManager * manager;
public:
	//managerにCManagerのアドレスを入れる
	CScene(CManager *pManager) { manager = pManager; };
	virtual ~CScene() {};
	//描画
	virtual void Update() {};
	//更新
	virtual void Render() {};
};

//シーン管理クラス
class CManager
{
	//キー
	char *mouse;
	VECTOR *mousePos;
public:
	//今のシーンのポインタ
	CScene * scene;
	//マウスの入力状態をmouse,座標をmousePosに移す
	CManager(char *pMouse, VECTOR *pMousePos) { mouse = pMouse; mousePos = pMousePos; };
	~CManager() { delete scene; };
	char *GetMouse() { return mouse; }
	VECTOR GetMousePos() { return *mousePos; }
	//描画
	void Update() { scene->Update(); };
	//更新
	void Render() { scene->Render(); };
};
