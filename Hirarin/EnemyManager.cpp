#include "EnemyManager.h"
#include "Enemy.h"
#include "Define.h"
//#include "AfterManager.h"
#include "math.h"

CEnemyManager::CEnemyManager()
{
	//NULLを入れて初期化
	for (int i = 0; i < ENEMY_NUM; i++)
	{
		enemy[i] = NULL;
	}
}

CEnemyManager::~CEnemyManager()
{
	for (int i = 0; i < ENEMY_NUM; i++)
	{
		delete enemy[i];
	}
}

void CEnemyManager::Update()
{
	//AfterSpawn();
	//生存フラグが倒れたEnemyをdeleteしNULLを入れて初期化する
	for (int i = 0; i < ENEMY_NUM; i++)
	{
		if (enemy[i] != NULL)
		{
			if (enemy[i]->GetFlag() == false)
			{
				delete enemy[i];
				enemy[i] = NULL;
			}
		}
	}

	//NULLでないenemyを更新させる
	for (int i = 0; i < ENEMY_NUM; i++)
	{
		if (enemy[i] != NULL)
		{
			enemy[i]->SetPlaPos(plaPos);
			enemy[i]->Update();
		}
	}
}

void CEnemyManager::Render()
{
	//NULLでないenemyを描画させる
	for (int i = 0; i < ENEMY_NUM; i++)
	{
		if (enemy[i] != NULL)
		{
			enemy[i]->Render();
		}
	}
}

void CEnemyManager::Spawn()
{
	cnt++;
	//30FごとにEnemyをランダムな場所で湧かせる
	if (cnt % 30 == 0)
	{
		cnt = 0;
		for (int i = 0; i < ENEMY_NUM; i++)
		{
			if (enemy[i] == NULL)
			{
				double width = 0;
				double height = 0;
				SpawnPos(&width, &height);
				VECTOR pos = VGet((float)width, (float)height, 0);
				enemy[i] = new EnemyA(&pos);
				enemy[i]->SetAfterManager(afterManager);
				break;
			}
		}
	}

}

void CEnemyManager::SpawnPos(double *width, double *height)
{
	int rand = GetRand(3);
	if (rand <= 1)
	{
		*width = GetRand(SCREEN_WIDTH);
		if (rand == 0)
		{
			*height = SCREEN_HEIGHT;
		}
	}
	else
	{
		*height = GetRand(SCREEN_HEIGHT);
		if (rand == 3)
		{
			*width = SCREEN_WIDTH;
		}
	}
}

//void CEnemyManager::AfterSpawn()
//{
//	for (int i = 0; i < ENEMY_NUM; i++)
//	{
//		if (enemy[i] != NULL)
//		{
//			VECTOR pos = enemy[i]->GetPos();
//			VECTOR vPos = enemy[i]->GetVPos();
//			afterManager->rotation(&pos, &vPos, 60, 5);
//			Spawn(&pos, &vPos);
//			afterManager->rotation(&pos, &vPos, 120, 5);
//			Spawn(&pos, &vPos);
//			afterManager->rotation(&pos, &vPos, 150, 5);
//			Spawn(&pos, &vPos);
//			afterManager->rotation(&pos, &vPos, 210, 5);
//			Spawn(&pos, &vPos);
//			afterManager->rotation(&pos, &vPos, 225, 5);
//			Spawn(&pos, &vPos);
//		}
//	}
//}

//void CEnemyManager::Spawn(VECTOR *pPos, VECTOR *pVPos)
//{
//	//afterManager->Spawn(pPos, pVPos);
//}

//void CEnemyManager::SetAfterManager(CAfterManager *after)
//{
//	for (int i = 0; i < ENEMY_NUM; i++)
//	{
//		enemy[i]->SetAfterManager(after);
//	}
//}