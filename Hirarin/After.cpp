#include "After.h"


CAfter::CAfter(VECTOR *pPos, SColor *pColor, float pCountMin)
{
	pos=*pPos;
	cnt = 0;
	//移動速度を0にする
	vPos = VGet(NULL, NULL, 0);
	color = *pColor;
	countMin = pCountMin;
}

CAfter::CAfter(VECTOR *pPos, VECTOR *pVPos, SColor *pColor, float pCountMin)
{
	pos = *pPos;
	cnt = 0;
	//与えられたベクトルを反転させる
	vPos = *pVPos;
	vPos = VScale(vPos, -1);
	color = *pColor;
	countMin = pCountMin;
}

void CAfter::Update()
{
	cnt++;
	pos = VAdd(pos, vPos);
}

void CAfter::Render()
{
	DrawPixel((int)pos.x, (int)pos.y, GetColor(color.red,color.green,color.blue));
	//DrawPixel((int)pos.x + 1, (int)pos.y, GetColor(P_COLOR));
	//DrawPixel((int)pos.x - 1, (int)pos.y, GetColor(P_COLOR));
	//DrawPixel((int)pos.x, (int)pos.y + 1, GetColor(P_COLOR));
	//DrawPixel((int)pos.x, (int)pos.y - 1, GetColor(P_COLOR));
}
