#include "Define.h"
#include "Manager.h"
#include "Game.h"

bool Procces(char *mouse, VECTOR *mousePos);
int GpUpdateMouse(char *mouse);
int GpUpdateMousePos(VECTOR *mouse);

// プログラムは WinMain から始まります
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	//ウインドウモードに変更
	ChangeWindowMode(TRUE);

	//DXライブラリを初期化する。
	if (DxLib_Init() == -1) return -1;

	//マウスのクリック状態を格納
	char mouse[MOUSE_NUM];
	//マウスの座標を格納
	VECTOR mousePos=VGet(0,0,0);

	//描画先画面を裏にする
	SetGraphMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32);

	SetDrawScreen(DX_SCREEN_BACK);

	//CManagerを動的確保しマウス情報を渡す
	CManager *manager;
	manager = new CManager(mouse, &mousePos);
	//managerに最初のシーンを渡す
	manager->scene = new CGame(manager);

	//ゲームループ
	while (Procces(mouse, &mousePos))
	{
		manager->Update();
		manager->Render();
	}
	
	delete manager;

	DxLib_End();				// ＤＸライブラリ使用の終了処理

	return 0;				// ソフトの終了 
}

bool Procces(char *mouse, VECTOR *mousePos)
{
	if (ProcessMessage() != 0) return false;
	//画面の裏ページの内容を表ページに反映する
	if (ScreenFlip() != 0) return false;
	//画面を初期化
	if (ClearDrawScreen() != 0) return false;
	//マウスの入力状態を反映
	if (GpUpdateMouse(mouse) != 0) return false;
	if (GpUpdateMousePos(mousePos) != 0) return false;
	//エスケープで終了
	if (CheckHitKey(KEY_INPUT_ESCAPE) >= 1) return false;
	return true;
}

//マウスがクリックされているかどうかをmouseに格納する
int GpUpdateMouse(char *mouse)
{
	if ((GetMouseInput() & MOUSE_INPUT_LEFT) != 0)
	{
		mouse[MOUSE_LEFT]++;
	}
	else
	{
		mouse[MOUSE_LEFT] = 0;
	}
	if ((GetMouseInput()&MOUSE_INPUT_RIGHT) != 0)
	{
		mouse[MOUSE_RIGHT]++;
	}
	else
	{
		mouse[MOUSE_RIGHT] = 0;
	}

	return 0;
}

//マウスの位置情報をmousePosに格納
int GpUpdateMousePos(VECTOR *mousePos)
{
	int x, y;

	GetMousePoint(&x, &y);

	mousePos->x = (float)x;
	mousePos->y = (float)y;

	return 0;
}