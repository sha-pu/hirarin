#include"Manager.h"
#include"Title.h"
#include"Define.h"
#include"Over.h"

void COver::Update()
{
	if (manager->GetMouse()[MOUSE_RIGHT] == 1)
	{
		manager->scene = new CTitle(manager);
		delete this;
	}
}

void COver::Render()
{
	DrawFormatString(SCREEN_W_HALF, SCREEN_H_HALF, GetColor(255, 255, 255), "オーバー");
	DrawFormatString(SCREEN_W_HALF, SCREEN_H_HALF+32, GetColor(255, 255, 255), "右クリックでタイトルへ");
}
