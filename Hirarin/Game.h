#pragma once

class CPlayer;
class CManager;
class CAfterManager;
class CEnemyManager;
class CCollision;

class CGame:public CScene
{
	CPlayer *player;
	CAfterManager *afterManager;
	CEnemyManager *enemyManager;
	CCollision *collision;
public:
	CGame(CManager *pManager);
	~CGame();
	void Update();
	void Render();

	void SceneChange();
	bool Debug_Scene();
};