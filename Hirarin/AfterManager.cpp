//#include "Define.h"
#include "AfterManager.h"
#include "math.h"

CAfterManager::CAfterManager()
{
	//NULLを入れて初期化
	for (int i = 0; i < AFTER_NUM; i++)
	{
		after[i] = NULL;
	}
}

CAfterManager::~CAfterManager()
{
	for (int i = 0; i < AFTER_NUM; i++)
	{
		delete after[i];
	}
}

void CAfterManager::Update()
{
	CountCheck();
	//NULLでないAfterを更新させる
	for (int i = 0; i < AFTER_NUM; i++)
	{
		if (after[i] != NULL)
		{
			after[i]->Update();
		}
	}
}

void CAfterManager::Render()
{
	//NULLでないAfterを描画させる
	for (int i = 0; i < AFTER_NUM; i++)
	{
		if (after[i] != NULL)
		{
			after[i]->Render();
		}
	}
}

void CAfterManager::Spawn(VECTOR *pPos, SColor *pColor, float pCountMin)
{
	for (int i = 0; i < AFTER_NUM; i++)
	{
		if (after[i] == NULL)
		{
			after[i] = new CAfter(pPos, pColor, pCountMin);
			break;
		}
	}
}

void CAfterManager::Spawn(VECTOR *pPos, VECTOR *vPos, SColor *pColor, float pCountMin)
{
	for (int i = 0; i < AFTER_NUM; i++)
	{
		if (after[i] == NULL)
		{
			after[i] = new CAfter(pPos, vPos, pColor, pCountMin);
			break;
		}
	}
}


void CAfterManager::CountCheck()
{
	for (int i = 0; i < AFTER_NUM; i++)
	{
		if (after[i] != NULL)
		{
			//移動速度が与えられた場合一定カウント+ランダムで得られるカウント分生存し、与えられなかった場合1Fで消える
			float cntRand = after[i]->GetCountMin();
			float cnt = 1;
			if (after[i]->GetVPpos().x != NULL && after[i]->GetVPpos().y != NULL)
			{
				cnt = (float)(GetRand(P_SA_CNT_RAND) + cntRand);
			}
			if (after[i]->GetCnt() >= cnt)
			{
				delete after[i];
				after[i] = NULL;
			}
		}
	}
}

void CAfterManager::rotation(VECTOR *sPos, const VECTOR *pos, const VECTOR *vPos, float rad, float x)
{
	*sPos = (VGet((*vPos).x + (float)cos(rad) * x, (*vPos).y + (float)sin(rad) * x, 0));
}